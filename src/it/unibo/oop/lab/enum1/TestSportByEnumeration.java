package it.unibo.oop.lab.enum1;

import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        /*
         * create 6 social network users
         */
        final SportSocialNetworkUserImpl<User> kbacon = new SportSocialNetworkUserImpl<>("Kevin", "Bacon", "kbacon", 56);
        kbacon.addSport(Sport.BASKET);
        kbacon.addSport(Sport.VOLLEY);
        kbacon.addSport(Sport.BIKE);
        

        System.out.println("Does kbacon follow " + Sport.BASKET + "? " + kbacon.hasSport(Sport.BASKET));
        System.out.println(kbacon.hasSport(Sport.BASKET) ? "PASS" : "FAIL");
        System.out.println("Does kbacon follow " + Sport.VOLLEY + "? " + kbacon.hasSport(Sport.VOLLEY));
        System.out.println(kbacon.hasSport(Sport.VOLLEY) ? "PASS" : "FAIL");
        System.out.println("Does kbacon follow " + Sport.MOTOGP + "? " + kbacon.hasSport(Sport.MOTOGP));
        System.out.println(kbacon.hasSport(Sport.MOTOGP) ? "FAIL" : "PASS");
    }

}
