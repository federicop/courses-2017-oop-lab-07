/**
 * 
 */
package it.unibo.oop.lab.enum2;

/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */
public enum Sport {

    /*
     * Declare the following sports:
     * 
     * - basket
     * 
     * - volley
     * 
     * - tennis
     * 
     * - bike
     * 
     * - F1
     * 
     * - motogp
     */

	BASKET(Place.INDOOR, 7, "BasketBall"),
	SOCCER(Place.OUTDOOR, 12, "Soccer"),
	TENNIS(Place.INDOOR, 1, "Tennis"),
	BIKE(Place.OUTDOOR, 1, "Bike"),
	F1(Place.OUTDOOR, 2, "F1"),
	MOTOGP(Place.OUTDOOR, 2, "MotoGP"),
	VOLLEY(Place.INDOOR, 10, "Volley");
	
    /*
     * 
     * [FIELDS]
     * 
     * Declare required fields
     */
	private final Place place;
	private final int noTeamMembers;
	private final String actualName;

    /*
     * 
     * [CONSTRUCTOR]
     * 
     * Define a constructor like this:
     * 
     * - Sport(final Place place, final int noTeamMembers, final String actualName)
     */
	/**
	 * 
	 * @param place
	 * 			Physical {@link Place} where it's played
	 * @param noTeamMembers
	 * 			Number of members in a single team (1 if it's an individual sport)
	 * @param actualName
	 * 			Human readable sport name
	 */
	private Sport(final Place place, final int noTeamMembers, final String actualName) {
		this.place = place;
		this.noTeamMembers = noTeamMembers;
		this.actualName = actualName;
	}

    /*
     * 
     * [METHODS] To be defined
     * 
     * 
     * 1) public boolean isIndividualSport()
     * 
     * Must return true only if called on individual sports
     * 
     * 
     * 2) public boolean isIndoorSport()
     * 
     * Must return true in case the sport is practices indoor
     * 
     * 
     * 3) public Place getPlace()
     * 
     * Must return the place where this sport is practiced
     * 
     * 
     * 4) public String toString()
     * 
     * Returns the string representation of a sport
     */
	
	/**
	 * 
	 * @return true if the sport team has 1 member only (IE: individual sport)
	 */
	public boolean isIndividualSport() {
		return noTeamMembers == 1;
	}
	
	/**
	 * 
	 * @return true if the sport is played indoor
	 */
	public boolean isIndoorSport() {
		return place == Place.INDOOR;
	}
	
	/**
	 * 
	 * @return the physical {@link Place} where it's played
	 */
	public Place getPlace() {
		return place;
	}
	
	/**
	 * @return a {@link String} representation
	 */
	public String toString() {
		return "Sport [actualName=" + actualName + ", place=" + place + ", noTeamMembers=" + noTeamMembers + "]";
	}
}
