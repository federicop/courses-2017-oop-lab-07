package it.unibo.oop.lab.nesting2;

import java.util.List;

/**
 * 
 * Represent a class of object accepting a given sequence ({@link List}) of elements following its order.
 * 
 * @param <T>
 */
public class OneListAcceptable<T> implements Acceptable<T> {
	private final List<T> elements;

	/**
	 * 
	 * @param elements
	 * 			The sequence ({@link List}) of elements to generate the {@link Acceptor} for
	 */
	public OneListAcceptable(final List<T> elements) {
		this.elements = elements;
	}

	/**
	 * {@inheritDoc}
	 */
	public Acceptor<T> acceptor() {
		return new Acceptor<T>() {
			private int pos = 0;

			/**
			 * {@inheritDoc}
			 */
			public void accept(final T newElement) throws ElementNotAcceptedException {
				if(pos >= elements.size() || !elements.get(pos).equals(newElement)) {
					throw new ElementNotAcceptedException(newElement);
				}
				pos++;
			}

			/**
			 * {@inheritDoc}
			 */
			public void end() throws EndNotAcceptedException {
				if(pos != elements.size()) {
					throw new EndNotAcceptedException();
				}
			}
		};
	}

}
